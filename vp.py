#!/usr/bin/env python

import sys
from fibheap import fibonacci_heap
from math import fabs, sqrt
import cProfile

sigma = 0.1
narrowband_radius = 2.5
LARGE = 1e9 

class Voxel:

    wys = {'up': [0,0,1],
            'down': [0,0,-1],
            'right': [1,0,0],
            'left': [-1,0,0],
            'forward': [0,1,0],
            'backward': [0,-1,0] }

    def __init__(self, (x, y, z), vl, tag, filament_i):
        self._coord = (x, y, z)
        self.vl = vl
        self.tag = tag
        self._filament_i = filament_i

    def __cmp__(self, other):
        return self.vl - other.vl

    #def __cmp__(self, other):
        #return self.coord() == other.coord()

    def __hash__(self):
        #return hash(self._coord)
        return hash(str(self._coord))

    def __str__(self):
        return '{coord:' + str(self.coord()) + ', vl:' + str(self.vl) + ', tag:\'' + self.tag + '\', filament:' + str(self.filament()) + '}'

    #def compare(self, other):
        #return self.vl - other.vl

    def coord(self, wy=None):
        if wy is None:
            return self._coord
        wys = self.wys
        return ( wys[wy][0]+self._coord[0], wys[wy][1]+self._coord[1], wys[wy][2]+self._coord[2] )
        
    def filament(self):
        return self._filament_i

    def print_neighbours(self):
        for wy in ['right', 'left', 'up', 'down', 'forward', 'backward']:
            print wy + ': ' + str( self.coord(wy)  )

def get_voxel_vl(voxel_dict, coord):
    if coord not in voxel_dict:
        return LARGE
    else:
        return (voxel_dict[coord]).vl

def solve_eikonal(voxel_dict, voxel):
    f = 1
    # Find neighbouring U's in upwind direction 
    u=[]
    for wys in [ ['right','left'], ['up','down'], ['forward','backward'] ]:
        vl_a = get_voxel_vl(voxel_dict, voxel.coord(wys[0]))
        vl_b = get_voxel_vl(voxel_dict, voxel.coord(wys[1]))
        u.append( min(vl_a, vl_b) )

    u.sort() # Order so that u1 < u2 < u3
    #print u
    #print "voxel " + str(voxel) + ", first u1,u2,u3 is " + str(u)
	
    # Solve the Eikonal equation for the current neighbouring point.
    #	The equation is (u-u1)^2+(u-u2)^2+(u-u3)^2 = f^2 = 1, with u >= u3 >= u2 >= u1.
    # =>    3*u^2 - 2*(u2+u1+u3)*u - f^2 + u1^2 + u3^2 + u2^2
    # => delta = (u2+u1+a3)^2 - 3*(a1^2 + a3^2 + a2^2 - P^2)
    delta = sum(u)**2 - 3*(u[0]**2 + u[1]**2 + u[2]**2 - f**2)
    #print 'delta is ' + str(delta)
    uNeigh = 0.0
    if delta >= 0.0:
        uNeigh = ( sum(u) + sqrt(delta) )/3.0
    if uNeigh <= u[2]:
        delta = (u[1]+u[0])**2 - 2*(u[0]**2 + u[1]**2 - f**2)
        uNeigh = 0.0
        if delta >= 0.0:
            uNeigh = 0.5 * (u[1]+u[0] + sqrt(delta) )
        if uNeigh <= u[1]:
            uNeigh = u[0] + f

    #print 'uNeigh is ' + str(uNeigh)
    return uNeigh


def print_neighbour_values(voxel_dict, voxel):
    for wy in ['right', 'left', 'up', 'down', 'forward', 'backward']:
        print wy + ' ' + str(voxel.coord(wy)) + ' ' + str( get_voxel_vl(voxel_dict, voxel.coord(wy) ) )
   

def fmm(voxel_heap_dict, fh, vl_most, filament_i, should_expand):
    #voxel_dict = {}
    while len(fh) > 0:
        #for key,value in fh._trees_by_item.items():
            #assert(value.item.tag is 'close' or value.item.tag is 'alive')
        if fh.peek().vl > vl_most or fabs(fh.peek().vl-vl_most) < 1e-6:
            print  'DT has reached bandwidth. min key = ' + str(fh.peek().vl) + ', vlBandwidth = ' + str(vl_most)
            break
        trial_voxel = fh.pop()
        #print 'popping min: ' + str(trial_voxel)
        trial_voxel.tag = 'alive'
        if (should_expand):
            #if trial_voxel.coord() not in voxel_heap_dict:
            voxel_heap_dict[trial_voxel.coord()] = trial_voxel
            #del voxel_heap_dict[trial_voxel.coord()]
        for wy in Voxel.wys: 
            has_neighbour = False
            neigh_vx = None
            if trial_voxel.coord(wy) in voxel_heap_dict:
                has_neighbour = True
                neigh_vx = voxel_heap_dict[ trial_voxel.coord(wy) ]
            elif should_expand:
                neigh_vx = Voxel(trial_voxel.coord(wy) , LARGE, 'far', filament_i)
                assert(not fh.__contains__(neigh_vx))
                voxel_heap_dict[neigh_vx.coord()] = neigh_vx
                has_neighbour = True
            
            if has_neighbour and neigh_vx.tag is not 'alive':
                #print '=========================='
                #print 'trial voxel: ' + str((voxel_heap_dict[trial_voxel.coord()]))
                #print 'neigh voxel: ' + str(voxel_heap_dict[neigh_voxel.coord()])
                #print '=========================='
                neigh_vx.vl = solve_eikonal(voxel_heap_dict, neigh_vx)
                if neigh_vx.tag is 'far':
                    if (fh.__contains__(neigh_vx)):
                        print 'fh contains the voxel: ' + str(neigh_vx)
                        print 'existing item is ' + str(fh._trees_by_item[neigh_vx].item)
                        #assert(not fh.__contains__(neigh_vx))
                    neigh_vx.tag = 'close'
                    fh.add(neigh_vx)
                    assert(neigh_vx in fh._trees_by_item)
                    #print 'Adding ' + str(neigh_vx) + ' to heap '
                elif neigh_vx.tag is 'close':
                    if neigh_vx not in fh._trees_by_item:
                        print neigh_vx
                    fh.decrease_key(neigh_vx, neigh_vx)
                else:
                    raise KeyError('Voxel should be either far or close')

    #return voxel_dict

def fmm_create_voxels(voxel_heap_dict, fh, vl_most, filament_i):
    return fmm(voxel_heap_dict, fh, vl_most, filament_i, True)

def fmm_not_create_voxels(fh, vl_most):
    return fmm(None, fh, vl_most, 0, False)

def insert_boundary_voxels_into_heap(fh, voxel_dict):
    for key,voxel in voxel_dict.items():
        voxel.vl = LARGE
        voxel.tg = 'far'
        for wy in ['up','down','left','right','forward','backward']:
            if voxel.coord(wy) not in voxel_dict: # i.e. if voxel is a boundary voxel
                voxel.vl = 0.0
                voxel.tg = 'alive'
                #print 'Adding ' + str(voxel) + ' to the heap'
                fh.add(voxel)
                break

def voxels_from_filaments(filament, filament_i, res):
    voxel_dict = {}
    print 'Adding ' + str(len(filament)) + ' nodes to dict'
    for node in filament:
        coordinates = (int(round(res*node[0])), int(round(res*node[1])), int(round(res*node[2])))
        voxel_dict[coordinates] = Voxel(coordinates, 0.0, 'alive', filament_i)
    print 'num voxels in voxel_dict is ' + str(len(voxel_dict))
    return voxel_dict

def write_to_vtk(voxel_dict, filename):
    num_voxels = len(voxel_dict.items())
    with open(filename, 'w') as f:
        f.write("# vtk DataFile Version 3.1\n")
        f.write("Created by raw2vtk.pl\n")
        f.write("ASCII\n")
        f.write("DATASET UNSTRUCTURED_GRID\n")
        f.write("POINTS " + str(num_voxels) + " FlOAT\n")
        for voxel in voxel_dict.values():
            f.write(str(voxel.coord()[0]) + ' ' +  str(voxel.coord()[1]) + ' ' + str(voxel.coord()[2]) + '\n')
            #f.write(str(voxel.coord()[0]) + ' ' +  str(voxel.coord()[1]) + ' ' + str(voxel.coord()[2]) + ' ' + str(voxel.vl) + '\n')
        f.write("\n")
        f.write("POINT_DATA " + str(num_voxels) + "\n")
        f.write("SCALARS NarrowBandValue float\n")
        f.write("LOOKUP_TABLE default\n")
        for voxel in voxel_dict.values():
            f.write(str(voxel.vl) + '\n')

def write_to_raw(voxel_dict, filename):
    with open(filename, 'w') as f:
        for voxel in voxel_dict.values():
            f.write(str(voxel.coord()[0]) + ' ' +  str(voxel.coord()[1]) + ' ' + str(voxel.coord()[2]) + ' ' + str(voxel.vl) + '\n')

def create_narrow_band(filament, filament_i, res):
    fh = fibonacci_heap()
    print '1. Rounding filament nodes to nearest voxels and inserting into voxel dictionary...'
    voxel_heap_dict = voxels_from_filaments(filament, filament_i, res)
    insert_boundary_voxels_into_heap(fh, voxel_heap_dict)
    #debug
    for key,voxel in voxel_heap_dict.items():
        assert(voxel.vl == 0.0)
        assert(voxel.tag == 'alive')
    #debug
    print 'size of fibonacci heap is ' + str(len(fh))
    print 'size of voxel dictionary is ' + str(len(voxel_heap_dict))
    #print 'min of fibonacci heap is ' + str(fh.peek().vl)
    tube_radius = sigma/2.0
    print 'Using tube radius = ' + str(tube_radius)
    cvx_nb_start = tube_radius*res - narrowband_radius;
    cvx_nb_end = tube_radius*res + narrowband_radius;
    print '2. Creating interior voxels (all voxels at FMM distance < ' + str(cvx_nb_start) + ' from voxel filament)...'
    #interior_voxels_dict = fmm_create_voxels(voxel_heap_dict, fh, cvx_nb_start, filament_i)
    #interior_voxels_dict = 
    fmm_create_voxels(voxel_heap_dict, fh, 5.0, filament_i)
    if not voxel_heap_dict:
        print 'No interior voxels were created'
    print len( voxel_heap_dict )
    write_to_vtk(voxel_heap_dict, 'tube.vtk')
    write_to_raw(voxel_heap_dict, 'tube.raw')
    
def main():
    filaments=[]
    for arg in sys.argv[1:]:
        with open(arg) as f:
            nodes = f.readlines()
            filaments.append([ tuple(map(float,node.strip().split())) for node in nodes ])
            print 'Adding filament with ' + str(len(filaments[-1])) + ' nodes'

    nb = []
    for filament_i,filament in enumerate(filaments):
        nb.append(create_narrow_band(filament, filament_i, 60.0))

if __name__ == "__main__":
    cProfile.run('main()', 'fooprof')
    main()

